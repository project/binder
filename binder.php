<?php
/**
 * @file
 * Description and discussion of the binder state machine.
 * @addtogroup binder
 * @{
 * Maybe a micromanagement exercise. Implements a state machine runner.
 * It is somewhat similar but simplified and computationally more powerful 
 * than the workflow module. It does not rely on explicitly named states,
 * it is not tied to node types in any manner, but by default to nodes.
 * The 'bindings' can be extended by implementing the hook_binder_schema().
 * Consider binder an alternative or complimentary workflow implementation,
 * which may peacefully coexist with and enhance the workflow module.
 *
 * The module intentionally does not provide any user interface. It is designed
 * to execute the state machine of a node. Defining it is a concern for
 * third party modules. There are too many possible ways to define the node
 * states, that it is feasible to provide a default one in binder.module 
 *
 * For example you can write an action, which adds actions to a node depending on
 * some conditions. Anoter possibility is to create a UI, explicitly adding actions
 * to a node. Yet another possibility is the actions (guards,events) to be managed
 * by workflow module.
 */


/** 
 * a hook to calculate and return the actions and guards (the schema) related to a node
 * @param $node a drupal node object, as used in nodeapi
 * @return array of events actions and guards for the node
 */
function hook_binder_schema(&$node) {
  
}
/**
 * @} End of "addtogroup binder".
 */

?>